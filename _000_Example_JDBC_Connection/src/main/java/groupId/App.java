package groupId;

import java.sql.*;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) throws SQLException {

        System.out.println("Assist".hashCode());
        System.out.println();

        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        Connection connection = DriverManager.
                getConnection("jdbc:mysql://localhost:3306/blinov_db.testphones?serverTimezone=UTC",
                        "blinov_db",
                        "blinov_db");

        Statement query = connection.createStatement();
        ResultSet rs = query.executeQuery("SELECT * FROM phonebook");

        while (rs.next()) {
            //System.out.println(rs.getString("lastname"));
            System.out.println(rs.getString("phone"));
        }

        System.out.println();

        /*String query2 = "SELECT * FROM phonebook WHERE IDPHONEBOOK = ?";
        try(PreparedStatement ps = connection.prepareCall(query2)) {

            System.out.println(connection.isClosed());
            ps.setString(1, "2");
            ResultSet rs2 = ps.executeQuery();

            UserMapper userMapper = new UserMapper();

            userMapper.extractFromResultSet(rs2);
            //System.out.println(user);

        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }
}
