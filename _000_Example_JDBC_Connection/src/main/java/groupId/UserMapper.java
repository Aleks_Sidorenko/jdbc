package groupId;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class UserMapper implements ObjectMapper<User> {
    @Override
    public User extractFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();

        user.setIdphonebook(rs.getInt("idphonebook"));
        user.setLastname(rs.getString("lastname"));
        user.setPhone(rs.getInt("phone"));

        return user;
    }

    @Override
    public User makeUnique(Map<Integer, User> existing, User entity) {
        existing.putIfAbsent(entity.getIdphonebook(), entity);

        return existing.get(entity.getIdphonebook());
    }
}
