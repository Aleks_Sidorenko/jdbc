package by.bsu.logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created on 03.02.2020 16:10.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 6 # транзакция по переводу денег со счета на счет # SingletonEngine.java */
public class SingletonEngine {

    private Connection connectionTo;
    private Connection connectionFrom;
    private static SingletonEngine instance = null;

    public synchronized static SingletonEngine getInstance() {
        if (instance == null) {
            instance = new SingletonEngine();
            instance.getConnectionTo();
            instance.getConnectionFrom();
        }
        return instance;
    }

    public Connection getConnectionFrom() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connectionFrom = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/blinov_db.testFrom?DISABLED&serverTimezone=UTC&user=blinov_db&password=blinov_db");
            connectionFrom.setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage()
                    + "SQLState: " + e.getSQLState());
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver not found");
        }
        return connectionFrom;
    }

    public Connection getConnectionTo() {
        final String connectToAdress = "jdbc:mysql://localhost:3306/blinov_db.testTo?serverTimezone=UTC";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connectionTo = DriverManager.getConnection(
                    connectToAdress,
                    "blinov_db",
                    "blinov_db");
            connectionTo.setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage()
                    + "SQLState: " + e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.err.println("Driver not found");
        }
        return connectionTo;
    }

    public void transfer(String summa) throws SQLException {
        Statement stFrom = null;
        Statement stTo = null;
        try {
            int sum = Integer.parseInt(summa);
            if (sum <= 0) {
                throw new NumberFormatException("less or equals zero");
            }
            stFrom = connectionFrom.createStatement();
            stTo = connectionTo.createStatement();
            // транзакция из 4-х запросов
            ResultSet rsFrom =
                    stFrom.executeQuery("SELECT balance from table_from");
            ResultSet rsTo =
                    stTo.executeQuery("SELECT balance from table_to");
            int accountFrom = 0;
            while (rsFrom.next()) {
                accountFrom = rsFrom.getInt(1);
            }
            int resultFrom = 0;
            if (accountFrom >= sum) {
                resultFrom = accountFrom - sum;
            } else {
                throw new SQLException("Invalid balance");
            }
            int accountTo = 0;
            while (rsTo.next()) {
                accountTo = rsTo.getInt(1);
            }
            int resultTo = accountTo + sum;
            stFrom.executeUpdate(
                    "UPDATE table_from SET balance=" + resultFrom);
            stTo.executeUpdate("UPDATE table_to SET balance=" + resultTo);
            // завершение транзакции
            connectionFrom.commit();
            connectionTo.commit();
            System.out.println("remaining on :" + resultFrom + " UAH");
            System.out.println("remaining on :" + resultTo + " UAH");
        } catch (SQLException e) {
            System.err.println("SQLState: " + e.getSQLState()
                    + "Error Message: " + e.getMessage());
            // откат транзакции при ошибке
            connectionFrom.rollback();
            connectionTo.rollback();
        } catch (NumberFormatException e) {
            System.err.println("Invalid summa: " + summa);
        } finally {
            if (stFrom != null) {
                try {
                    stFrom.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (stTo != null) {
                try {
                    stTo.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
