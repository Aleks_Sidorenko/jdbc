package by.bsu;

import by.bsu.logic.SingletonEngine;

import java.sql.SQLException;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Transfer from accounts.");

        SingletonEngine singletonEngine = new SingletonEngine();
        SingletonEngine.getInstance();

        singletonEngine.getConnectionFrom();
        singletonEngine.getConnectionTo();

        try {
            singletonEngine.transfer("100");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
