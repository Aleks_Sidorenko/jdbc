package by.bsu;

import by.bsu.pool.ConnectionPool;
import by.bsu.simpledao.AbonentDAO;
import by.bsu.subject.Abonent;

import java.sql.SQLException;
import java.util.List;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) throws SQLException {
        AbonentDAO abonentDAO = new AbonentDAO();
        ConnectionPool.getConnection();

        List<Abonent> abonents = abonentDAO.findAll();
        System.out.println(abonents.toString());
    }
}
