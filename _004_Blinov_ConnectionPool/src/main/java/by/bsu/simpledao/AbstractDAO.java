package by.bsu.simpledao;

import by.bsu.subject.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created on 04.02.2020 22:37.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 7 # общие методы взаимодействия с моделью данных # AbstractDAO.java */

public abstract class AbstractDAO<K, T extends Entity> {

    public abstract List<T> findAll();
    public abstract T findEntityById(K id);
    public abstract boolean delete(K id);
    public abstract boolean delete(T entity);
    public abstract T create(T entity);
    public abstract T update(T entity);

    public void close(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
        // лог о невозможности закрытия Statement
        }
    }

    public void close(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
        // генерация исключения, т.к. нарушается работа пула
        }
    }
}
