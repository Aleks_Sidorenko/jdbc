package by.bsu.data.connect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import by.bsu.data.subject.Abonent;

/**
 * Created on 03.02.2020 14:01.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 4 # подготовка запроса на добавление информации # DataBaseHelper.java */
public class DataBaseHelper {

    //private final static String SQL_INSERT = "INSERT INTO phonebook(idphonebook, lastname, phone ) VALUES(?,?,?)";
    private Connection connect;
    private ResourceBundle resource = ResourceBundle.getBundle("database");

    public DataBaseHelper() throws SQLException {
        try {
            connect = ConnectorDB.getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement getPreparedStatement() {
        PreparedStatement ps = null;
        try {
            ps = connect.prepareStatement(resource.getString("db.insert_phonebook"));
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return ps;
    }

    public boolean insertAbonent(PreparedStatement ps, Abonent ab) {
        boolean flag = false;
        try {
            ps.setInt(1, ab.getId());
            ps.setString(2, ab.getLastname());
            ps.setInt(3, ab.getPhone());
            ps.executeUpdate();
            flag = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public void closeStatement(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
