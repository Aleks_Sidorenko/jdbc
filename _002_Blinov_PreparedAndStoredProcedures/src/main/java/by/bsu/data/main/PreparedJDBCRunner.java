package by.bsu.data.main;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import by.bsu.data.connect.DataBaseHelper;
import by.bsu.data.subject.Abonent;
/**
 * Created on 03.02.2020 14:06.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 5 # добавление нескольких записей в БД # PreparedJDBCRunner.java */
public class PreparedJDBCRunner {

    public static void main(String[] args) {
        ArrayList<Abonent> list = new ArrayList<Abonent>() {
            {
                add(new Abonent(3, 9876540, "David Syd"));
                add(new Abonent(4, 8546210, "Alec Syd"));
                add(new Abonent(5, 8546874, "Anna Syd"));
            }
        };

        DataBaseHelper helper = null;
        PreparedStatement statement = null;
        try {
            helper = new DataBaseHelper();
            statement = helper.getPreparedStatement();
            for (Abonent abonent : list) {
                helper.insertAbonent(statement, abonent);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            helper.closeStatement(statement);
        }
    }
}
