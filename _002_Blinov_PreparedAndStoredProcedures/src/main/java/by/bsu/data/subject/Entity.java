package by.bsu.data.subject;

import java.io.Serializable;

/**
 * Created on 03.02.2020 12:21.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 2 # класс с информацией # Entity.java # Abonent.java */
public abstract class Entity implements Serializable, Cloneable {

    private int id;

    public Entity() {
    }

    public Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
