package by.bsu.data.training;

import by.bsu.data.subject.Abonent1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created on 07.05.2021 0:07.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class Main1 {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/blinov_db.testphones?serverTimezone=UTC";
        Properties prop = new Properties();
        prop.put("user", "blinov_db");
        prop.put("password", "blinov_db");
        prop.put("autoReconnect", "true");
        prop.put("characterEncoding", "UTF-8");
        prop.put("useUnicode", "true");

        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        Connection cn = DriverManager.getConnection(url, prop);
        Statement st = cn.createStatement();

        ResultSet rs = st.executeQuery("SELECT * FROM phonebook");
        ArrayList<User> lst = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int phone = rs.getInt(3);
            lst.add(new User(id, phone, name));
        }
        if (lst.size() > 0) {
            System.out.println(lst);
        } else {
            System.out.println("Not found");
        }

        if (rs != null) { // был ли создан ResultSet
            rs.close();
        } else {
            System.err.println("ошибка во время чтения из БД");
        }

        if (st != null) { // для 2-го блока try
            st.close();
        } else {
            System.err.println("Statement не создан");
        }

        if (cn != null) {
            try {
                cn.close();
            } catch (SQLException e) {
                System.err.println("Connection close error: " + e);
            }
        }
    }
}