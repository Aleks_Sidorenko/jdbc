package by.bsu.data.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import by.bsu.data.subject.Abonent1;

/**
 * Created on 06.05.2021 23:09.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class SimpleJDBCRunner {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/blinov_db.testphones?serverTimezone=UTC";
        Properties prop = new Properties();
        prop.put("user", "blinov_db");
        prop.put("password", "blinov_db");
        prop.put("autoReconnect", "true");
        prop.put("characterEncoding", "UTF-8");
        prop.put("useUnicode", "true");
        Connection cn = null;
        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

        try { // 1. Block
            cn = DriverManager.getConnection(url, prop);
            Statement st = null;

            try { // 2. Block
                st = cn.createStatement();
                ResultSet rs = null;

                try { // 3. Block
                    rs = st.executeQuery("SELECT * FROM phonebook");
                    ArrayList<Abonent1> lst = new ArrayList<>();
                    while (rs.next()) {
                        int id = rs.getInt(1);
                        String name = rs.getString(2);
                        int phone = rs.getInt(3);
                        lst.add(new Abonent1(id, phone, name));
                    }
                    if (lst.size() > 0) {
                        System.out.println(lst);
                    } else {
                        System.out.println("Not found");
                    }
                } finally { // 3. Block Close
                    if (rs != null) { // был ли создан ResultSet
                        rs.close();
                    } else {
                        System.err.println("ошибка во время чтения из БД");
                    }
                } // 3. Block Close

            } finally { // 2. Block Close
                if (st != null) { // для 2-го блока try
                    st.close();
                } else {
                    System.err.println("Statement не создан");
                }
            } // 2. Block Close

        } catch (SQLException e) { // 1. Close Block
            System.err.println("DB connection error: " + e);
        } finally { // 1. Close Block
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException e) {
                    System.err.println("Connection close error: " + e);
                }
            }
        } // 1. Close Block
    }
}
