package by.bsu.data.subject;

/**
 * Created on 06.05.2021 23:15.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class Abonent1 extends Entity1 {

    private int phone;
    private String lastname;
    public Abonent1() {
    }
    public Abonent1(int id, int phone, String lastname) {
        super(id);
        this.phone = phone;
        this.lastname = lastname;
    }
    public int getPhone() {
        return phone;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    @Override
    public String toString() {
        return "Abonent [id=" + getId() + ", phone=" + phone +
                ", lastname=" + lastname + "]";
    }
}
