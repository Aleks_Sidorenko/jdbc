package by.bsu.subject;

/**
 * Created on 03.02.2020 12:24.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class Abonent extends Entity {

    private int phone;
    private String lastName;
    public Abonent() {
    }
    public Abonent(int id, int phone, String lastname) {
        super(id);
        this.phone = phone;
        this.lastName = lastname;
    }
    public int getPhone() {
        return phone;
    }
    public void setPhone(int phone) {
        this.phone = phone;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @Override
    public String toString() {
        return "Abonent [id=" + getId() + ", phone=" + phone +
                ", lastName=" + lastName + "]";
    }
}
