package by.bsu.simpledao;

import by.bsu.action.WrapperConnector;

import java.sql.Statement;

/**
 * Created on 04.02.2020 23:38.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */

/* # 10 # DAO с полем Connection # AbstractDAO.java */

public abstract class AbstractDAO {

    protected WrapperConnector connector;
    // методы добавления, поиска, замены, удаления
// методы закрытия коннекта и Statement
    public void close() {
        connector.closeConnection();
    }
    protected void closeStatement(Statement statement) {
        connector.closeStatement(statement);
    }
}
