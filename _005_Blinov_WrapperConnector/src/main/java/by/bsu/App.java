package by.bsu;

import by.bsu.action.WrapperConnector;
import by.bsu.simpledao.AbonentDAO;
import by.bsu.subject.Abonent;

import java.sql.SQLException;
import java.util.List;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) throws SQLException {
        AbonentDAO abonentDAO = new AbonentDAO();
        new WrapperConnector().getStatement();

        List<Abonent> list = abonentDAO.findAll();
        System.out.println(list.toString());
        new WrapperConnector().closeConnection();
    }
}
